package com.wsb.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class SparkNamesImporter {

    public static void main(String[] args) {

        Dataset<Row> df = readPersonDataFromCSV();

        showSpecificColumnData(df, "JobTitle");

        writeDataToDb(df);

    }

    private static Dataset<Row> readPersonDataFromCSV() {
        SparkSession spark = initSpark();

        Map<String, String> optionsMap = new java.util.HashMap<>();
        optionsMap.put("header", "true");

        String path = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("people-10000.csv")).getPath();

        Dataset<Row> df = spark.read().options(optionsMap).csv(path);
        df.show();
        return df;
    }

    private static void writeDataToDb(Dataset<Row> df) {
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", "root");
        connectionProperties.put("password", "qwerty");
        df.write().jdbc("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC", "sakila.wsb_person_45", connectionProperties);
    }

    private static void showSpecificColumnData(Dataset<Row> df, String jobTitle) {
        df.select("Job Title").distinct().show();
    }

    private static SparkSession initSpark() {
        return SparkSession.builder()
                           .appName("Java Spark SQL basic example")
                           .config("spark.csv.header", "true")
                           .config("spark.master", "local")
                           .getOrCreate();
    }
}
